## LinFlow-acoustic coupling

Solving the thermoviscous acoustic formulation is only necessary when thermal and viscous boundary layers have significant effects (i.e., near walls).
In the regions where no viscous effects occur, the standard wave equation ([Acoustic PDE](../../Singlefield/AcousticPDE)) can be used, which only has one scalar unknown and is computationally more efficient than [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md). The coupling between these two PDEs will be discussed here. 

### Coupling conditions
Following transmission conditions along the acoustic-LinFlow interface $\Gamma_i$ have to be fulfilled


\begin{eqnarray}
\mathbf \sigma_\mathrm{a} \cdot \mathbf n =  - p_\mathrm{a} \mathbf n = 
\mathbf \sigma_\mathrm{v} \cdot \mathbf n  \label{eq:trans1} \\
\mathbf v_\mathrm{a} \cdot \mathbf n = \mathbf v_\mathrm{v} \cdot \mathbf n\,,\label{eq:trans2}
\end{eqnarray}

where $\sigma_\mathrm{a}$, $\sigma_\mathrm{v}$, $p_\mathrm{a}$, $v_\mathrm{a}$, and  $v_\mathrm{v}$ are acoustic stress tensor, LinFlow viscous stress tensor, acoustics pressure, acoustics velocity and LinFlow velocity, respectively.
The continuity of pressure means that the net force should be zero on the (massless) interface separating the fluids. Note that this coupling transmission is not valid where the viscous boundary layer exists. 
The continuity of the normal component of velocity implies that the fluid should stay in contact. 


We assume region $\Omega_\mathrm{a}$ is modeled by the
acoustics PDE (assuming non-viscous fluid) and
$\Omega_\mathrm{v}$ by the LinFlow PDE and the normal vectors fulfill the
following relations
\begin{equation}
\mathbf n = \mathbf n_\mathrm{a} = - \mathbf n_\mathrm{v}\,.
\end{equation}

### Weak formulation
The first transmission equation according to \eqref{eq:trans1} can be
directly used and substituted into the balance of momentum in [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md) so that the surface term gets the form

\begin{equation}
\label{eq:trans1Surf}
 - \int\limits_{\Gamma_i} \mathbf v' \cdot \mathbf
 \sigma_\mathrm{v} \cdot \mathbf n_\mathrm{v} {\mathrm {d}} \Gamma = -\int\limits_{\Gamma_i} \mathbf v' \cdot \mathbf
 n \, p_\mathrm{a} \, {\mathrm {d}} \Gamma\,.
\end{equation}

Furthermore, the conservation of momentum for acoustics in $\Omega_\mathrm{a}$ (non-viscous fluid)
reads as
\begin{equation}
\nabla p_\mathrm{a} = - \rho_0 \frac{\partial \mathbf v_\mathrm{a}}{\partial
    t}\,.
\end{equation}
This relation and \eqref{eq:trans2} allows to rewrite the surface term
in the weak form of [Acoustic PDE](../../Singlefield/AcousticPDE) as follows

\begin{equation}
- \int\limits_{\Gamma_i}
   \varphi\, \mathbf n_\mathrm{a} \cdot \nabla p_\mathrm{a}\, {\mathrm {d}} \Gamma =
 \int\limits_{\Gamma_i}
   \rho_0\, \varphi \, \frac{\partial \mathbf v_\mathrm{v}}{\partial t} \cdot
   \mathbf n\, {\mathrm {d}} \Gamma\,.
\end{equation}


### Analysis Types
Same as [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md) and [Acoustic PDE](../../Singlefield/AcousticPDE), the coupling can be solved in harmonic transient studies.

* **Harmonic**

*not validated yet:* **Transient** 


### Defining the coupling
LinFlow-acoustics coupling benefits form [non-conforming](../../../Tutorials/Features/ncinterfaces.md) interfaces option.
Therefore in the simulation xml-file, the non-conforming interface option needs to be defined as

```
    <domain geometryType=""> 
        <regionList>
            <region name="" material=""/>
        </regionList> 
        <surfRegionList>
            <surfRegion name="LinFLow"/>
            <surfRegion name="acoustics"/>
        </surfRegionList>        
        <ncInterfaceList>
            <ncInterface name="LinFLow_acou_interface" masterSide="LinFLow" slaveSide="acoustics"/>
        </ncInterfaceList>
    </domain>
```
Note that in this coupling, the LinFlow interface is considered as the ```  masterSide``` and the acoustics interface is considered as the ```slaveSide```.
In the direct coupling section the non-conforming interface defined in ``` <ncInterfaceList> ``` will be entered as

```
        <couplingList>
            <direct>

                <linFlowAcouDirect>
                    <ncInterfaceList > 
                        <ncInterface name="LinFLow_acou_interface"/>
                    </ncInterfaceList>
                </linFlowAcouDirect>
                
            </direct>
        </couplingList>
```

<!-- ### Material and postprocessing results

In openCFS material-xml file beside the flow material input for the [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md), an acoustic material input for the [Acoustic PDE](../../Singlefield/AcousticPDE) should also be defined.
```
  <material name="Air">

    <acoustic>
      ...
    </acoustic>

    <flow>
      ...
    </flow>

  </material>
```
The postprocessing results and the boundary conditions for each PDE are described in the "Singlefield" section.--> 
