Meshing
=======

You need an external mesh generator to create meshes for openCFS.
openCFS support a range of mesh formats as input, most widely used are:

* Ansys CDB (written by cubit / trelis)
* [Gmsh](https://gmsh.info/) (format < 3)
* cfs internal `*.mesh` files written by various preprocessing scripts

The principal mechanism by which openCFS interfaces with the mesh generator is by _region names_.
These _region names_ are assigned in the mesh generator and collect sets of _volume_ or _surface elements_ or even _nodes_.
The _region names_ are then used in the openCFS XML input to define PDEs or assign material properties on certain _volume regions_, apply boundary conditions to _surface regions_ or request results.
Once you have exported the mesh file from the mesh gernator, specify it as the `fileName` attribute it in respective mesh format tag in the `<fileFormats><input>` section of the opeCFS XML input.

In the following you'll find a few examples for selected mesh generators.
